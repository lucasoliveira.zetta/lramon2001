<p align="center">
<img src="LucasRamonSoftwareEngineerProfile.png">
</p>

![](https://komarev.com/ghpvc/?username=lramon2001&color=blue&style=flat-square)
[![Linkedin: lucas-ramon-alves-de-oliveira](https://img.shields.io/badge/linkedin-%230077B5.svg?&style=flat-square&logo=linkedin&logoColor=white)](https://www.linkedin.com/in/lucas-ramon-alves-de-oliveira/)
[![GitHub lramon2001](https://img.shields.io/github/followers/lramon2001?label=follow&style=social)](https://github.com/lramon2001)
[![Gmail Badge](https://img.shields.io/badge/-lucasoliveirainor3105@gmail.com-red?style=flat-square&logo=Gmail&logoColor=white&link=mailto:lucasoliveirainor3105@gmail.com)](mailto:nociamrq27@gmail.com)


## I'm currently studying:
![Java](https://img.shields.io/badge/-java-grey?style=for-the-badge&logo=java&logoColor=white&labelColor=blue)
![HTML5](https://img.shields.io/badge/html%205-grey?style=for-the-badge&logo=html5&logoColor=white&labelColor=blue)
![CSS3](https://img.shields.io/badge/css%203-grey?style=for-the-badge&logo=css3&logoColor=white&labelColor=blue)



<details>
<summary><b><i>More Skills</i></b></summary>
  
### to delve into the future:  
![Sass](https://img.shields.io/badge/sass-grey?style=for-the-badge&logo=sass&logoColor=white&labelColor=blue)
![Bootstrap](https://img.shields.io/badge/-bootstrap-grey?style=for-the-badge&logo=bootstrap&logoColor=white&labelColor=blue)
![WebPack](https://img.shields.io/badge/-webpack-grey?style=for-the-badge&logo=webpack&logoColor=white&labelColor=blue)


![python](https://img.shields.io/badge/-python-grey?style=for-the-badge&logo=python&logoColor=white&labelColor=blue)
![php](https://img.shields.io/badge/-php-grey?style=for-the-badge&logo=php&logoColor=white&labelColor=blue)
![javascript](https://img.shields.io/badge/-javascript-grey?style=for-the-badge&logo=javascript&logoColor=white&labelColor=blue)
![Assembly Mips](https://img.shields.io/badge/-mips-grey?style=for-the-badge&logo=nintendo&logoColor=white&labelColor=blue)

### that I already own:
![git](https://img.shields.io/badge/-git-grey?style=for-the-badge&logo=git&logoColor=white&labelColor=blue)
![github](https://img.shields.io/badge/-github-grey?style=for-the-badge&logo=github&logoColor=white&labelColor=blue)
![MarkDown](https://img.shields.io/badge/-Markdown-grey?style=for-the-badge&logo=Markdown&logoColor=white&labelColor=blue)
![C](https://img.shields.io/badge/-C_Language-grey?style=for-the-badge&logo=c&logoColor=white&labelColor=blue)

</details>
